package org.voiceofsandiego;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class InfoWindowLayout extends FrameLayout {
    public InfoWindowLayout(Context context) {
        this(context, null, 0);
    }

    public InfoWindowLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InfoWindowLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.info_window_view, this);
        TextView titleTextView = (TextView) findViewById(R.id.info_window_title);
        titleTextView.setText("Info Window Custom View");
    }

}
